#!/bin/bash

if [[ "$PWD" == *"/CMakeFiles/CMakeTmp" ]]; then
    # disable distcc for the calls during CMake configure
    if [[ "$COMPILER_PREFIX" == *"distcc" ]]; then
        unset COMPILER_PREFIX
    elif [[ "$CCACHE_PREFIX" == *"distcc" ]]; then
        unset CCACHE_PREFIX
    fi
fi

args=()
[ -n "$COMPILER_PREFIX" ] && args+=("$COMPILER_PREFIX")

# Since CMake 3.21 the ninja generator uses aboslute paths in the
# arguments passed to the compiler [1]. In principle we can deal
# with this by using ccache's base_dir, however, it seems that it
# handles symlinks in paths by canonicalizing them [2], which is
# not what we want in order to share the cache between in-source
# and out-of-source builds.
# Instead, we will just do the prefix removal ourselves here,
# which keeps the symlink and the paths look identical independent
# of where the build directory is.
# Note that we don't handle -isystem includes from upstream projects
# we built, since base_dir works fine for the InstallArea-s.
#
# [1] https://gitlab.kitware.com/cmake/cmake/-/merge_requests/6148
# [2] https://www.mail-archive.com/ccache@lists.samba.org/msg01385.html

if [[ "$BUILDDIR" == "$PWD" ]]; then
    for arg in "$@"; do
        arg="${arg/#-I$BUILDDIR\//-I}"  # substitute "-I$BUILDDIR/x/y.h" with "x/y.h"
        arg="${arg/#$BUILDDIR\//}"   # handle "$BUILDDIR/x/z.cpp" and "$BUILDDIR/toolchain/g++..."
        args+=($arg)
    done
else
    args+=("$@")
fi

exec "${args[@]}"
