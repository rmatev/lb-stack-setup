#!/bin/bash
set -eo pipefail

. $(dirname $0)/install-common.sh

setup 'https://gitlab.cern.ch/rmatev/distcc.git' 'master-fd943af-lhcb'
(
    ./autogen.sh
    ./configure --prefix $CONTRIB --with-auth --without-libiberty --disable-Werror
    make install -j$(nproc)
)
cleanup
