set -euxo pipefail

PLATFORM=$(utils/config.py binaryTag)  # make sure we don't use BINARY_TAG or CMTCONFIG
BUILD_PATH=$(utils/config.py buildPath)

# # avoid building tests (GaudiKernel/tests/src/parsers.cpp compilation is killed in CI)
# utils/config.py -- cmakeFlags.Gaudi '-DBUILD_TESTING=OFF'
# # disable LHCb tests too (GaudiAlgExamples depends on Gaudi::GaudiExamplesLib)
# utils/config.py -- cmakeFlags.LHCb '-DBUILD_TESTING=OFF'
# FIXME the above doesn't work because of missing if(BUILD_TESTING) in LHCb

utils/config.py  # print config
make Gaudi/configure
bash utils/ci-utils/test-cmake.sh $BUILD_PATH/Gaudi/build.$PLATFORM

# Compile memory greedy compilation units with fewer threads
# until we succeed.
jobs=$(nproc)
while [[ $jobs -ge 1 ]] ; do
  make BUILDFLAGS="-j$jobs" Gaudi/Parsers_test && break || true
done

make Gaudi
make Gaudi/configure  # test a reconfigure
bash utils/ci-utils/test-cmake.sh $BUILD_PATH/Gaudi/build.$PLATFORM
make Detector/configure
bash utils/ci-utils/test-cmake.sh $BUILD_PATH/Detector/build.$PLATFORM
make LHCb/configure
bash utils/ci-utils/test-cmake.sh $BUILD_PATH/LHCb/build.$PLATFORM
bash utils/ci-utils/test-misc.sh LHCb
bash utils/ci-utils/test-test.sh

utils/stats
